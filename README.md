# thefirst -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/thefirst)

## Chal Info

Desc: `Get warmed up, we'll be here for a while.`

Given files:

* thefirst

Hints:

* Notice any interesting functions lying around?

Flag: `TUCTF{0n3_d0wn..._50_m4ny_70_60}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/thefirst)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/thefirst:tuctf2019
```
